# Set modelname and path to Pflow preprocessing config file
model_name: models/training/DL1-PFlow_no_sv_pdf_250
preprocess_config: /home/jcrosby/work/umami/umami_dl1/models/training/DL1-PFlow_no_sv_pdf_250/metadata/PFlow-Preprocessing-dl1.yaml

# Add here a pretrained model to start with.
# Leave empty for a fresh start
model_file:

# Add training file
train_file: /home/jcrosby/eos/runs/dl1_no_sv_pdf/preprocessed/preprocessed_output/PFlow-hybrid-resampled_scaled_shuffled.h5

# Defining templates for the variable cuts
.variable_cuts_ttbar: &variable_cuts_ttbar
    variable_cuts:
        - pt_btagJes:
            operator: "<="
            condition: 2.5e5

.variable_cuts_zpext: &variable_cuts_zpext
    variable_cuts:
        - pt_btagJes:
            operator: ">"
            condition: 2.5e5

# Add validation files
validation_files:
    ttbar_r21_val:
        path: /home/jcrosby/eos/runs/dl1_no_sv_pdf/hybrids/MC16d-inclusive_validation_ttbar_PFlow.h5
        label: "$t\\bar{t}$ Release 21"
        <<: *variable_cuts_ttbar

    zprime_r21_val:
        path: /home/jcrosby/eos/runs/dl1_no_sv_pdf/hybrids/MC16d-inclusive_testing_zprime_PFlow.h5
        label: "$Z'$ Release 21"
        <<: *variable_cuts_zpext

test_files:
    ttbar_r21:
        path: /home/jcrosby/eos/runs/dl1_no_sv_pdf/hybrids/MC16d-inclusive_testing_ttbar_PFlow.h5
        <<: *variable_cuts_ttbar

    ttbar_r22:
        path: /home/jcrosby/eos/runs/dl1_no_sv_pdf/hybrids/MC16d-inclusive_testing_ttbar_PFlow.h5
        <<: *variable_cuts_ttbar

    zpext_r21:
        path: /home/jcrosby/eos/runs/dl1_no_sv_pdf/hybrids/MC16d-inclusive_testing_zprime_PFlow.h5
        <<: *variable_cuts_zpext

    zpext_r22:
        path: /home/jcrosby/eos/runs/dl1_no_sv_pdf/hybrids/MC16d-inclusive_testing_zprime_PFlow.h5
        <<: *variable_cuts_zpext

  #  dl1_no_sv_ttbar_r21:
  #      path: /home/jcrosby/eos/runs/training/no_ip/hybrids/MC16d-inclusive_testing_ttbar_PFlow.h5
  #      <<: *variable_cuts_ttbar

  # dl1_no_sv_zpext_r21:
  #      path: /home/jcrosby/eos/runs/training/no_ip/hybrids/MC16d-inclusive_testing_zprime_PFlow.h5
  #      <<: *variable_cuts_zpext

  # dl1_no_sv_ttbar_r21:
  #      path: /home/jcrosby/eos/runs/training/no_jf/hybrids/MC16d-inclusive_testing_ttbar_PFlow.h5
  #      <<: *variable_cuts_ttbar

  # dl1_no_sv_zpext_r21:
  #      path: /home/jcrosby/eos/runs/training/no_jf/hybrids/MC16d-inclusive_testing_zprime_PFlow.h5
  #      <<: *variable_cuts_zpext

  # dl1_no_sv_ttbar_r21:
  #      path: /home/jcrosby/eos/runs/training/no_sv/hybrids/MC16d-inclusive_testing_ttbar_PFlow.h5
  #      <<: *variable_cuts_ttbar

  # dl1_no_ip_zpext_r21:
  #      path: /home/jcrosby/eos/runs/training/no_ip/hybrids/MC16d-inclusive_testing_zprime_PFlow.h5
  #      <<: *variable_cuts_zpext     

# Path to Variable dict used in preprocessing
var_dict: /home/jcrosby/work/umami/umami_dl1/models/training/DL1-PFlow_no_sv_pdf_250/metadata/DL1_Variables.yaml

exclude: null

# Tracks dataset name
tracks_name: "tracks"

# Values for the neural network
NN_structure:
    # Decide, which tagger is used
    tagger: "dl1"

    # NN Training parameters
    lr: 0.001
    batch_size: 15000
    epochs: 250

    # Number of jets used for training
    # To use all: Fill nothing
    nJets_train:

    # Dropout rate. If = 0, dropout is disabled
    dropout: 0

    # Define which classes are used for training
    # These are defined in the global_config
    class_labels: ["ujets", "cjets", "bjets"]

    # Main class which is to be tagged
    main_class: "bjets"

    # Decide if Batch Normalisation is used
    Batch_Normalisation: True

    # Structure of the dense layers for each track
    ppm_sizes: [100, 100, 128]

    # Structure of the dense layers after summing up the track outputs
    dense_sizes: [100, 100, 100, 30]

    # Activations of the layers. Starting with first dense layer.
    activations: ["relu", "relu", "relu", "relu", "relu", "relu", "relu", "relu"]

    # Options for the Learning Rate reducer
    LRR: True

    # Option if you want to use sample weights for training
    use_sample_weights: False

# Plotting settings for training metrics plots
Validation_metrics_settings:
    # Number of jets used for validation
    n_jets: 3e5

    # Define which taggers should also be plotted
    taggers_from_file:
       IP3D: "Recomm. IP3D"
       DL1_no_ip: "Recomm. DL1_no_sv"

    # Label for the freshly trained tagger
    #tagger_label: "DL1"

    # Working point used in the validation
    WP: 0.77

    # Plotting API parameters
    # fc_value and WP_b are autmoatically added to the plot label
    atlas_first_tag: "Simulation Internal"
    atlas_second_tag: "\n$\\sqrt{s}=14$ TeV, PFlow jets"

    # Set the datatype of the plots
    plot_datatype: "png"

# Eval parameters for validation evaluation while training
Eval_parameters_validation:
    # Number of jets used for evaluation
    n_jets: 3e5

    # Define taggers that are used for comparison in evaluate_model
    # This can be a list or a string for only one tagger
    tagger: ["IP3D","DL1_no_sv"]
    
    # efficiency min / max
    eff_min: 0.49 #default 0.49
    eff_max: 1.0

    # Define fc values for the taggers
    frac_values_comp:
      {
         "IP3D": {"cjets": 0.07, "ujets": 0.93},
         "DL1_no_sv": {"cjets": 0.018, "ujets": 0.982}
       }

    # Charm fraction value used for evaluation of the trained model
    frac_values: {"cjets": 0.005, "ujets": 0.995}

    # A list to add available variables to the evaluation files
    add_variables_eval: ["actualInteractionsPerCrossing","HadronConeExclTruthLabelID"]

    # Working point used in the evaluation
    WP: 0.77

    # Decide, if the Saliency maps are calculated or not.
    calculate_saliency: True

     # some properties for the feature importance explanation with SHAPley
     #shapley:
        # Over how many full sets of features it should calculate over.
        # Corresponds to the dots in the beeswarm plot.
        # 200 takes like 10-15 min for DL1r on a 32 core-cpu
        #  feature_sets: 200
     
        # defines which of the model outputs (flavor) you want to explain
        # [tau,b,c,u] := [3, 2, 1, 0]
        #model_output: 2
     
        # You can also choose if you want to plot the magnitude of feature
        # importance for all output nodes (flavors) in another plot. This
        # will give you a bar plot of the mean SHAP value magnitudes.
        #bool_all_flavor_plot: False
     
        # as this takes much longer you can average the feature_sets to a
        # smaller set, 50 is a good choice for DL1r
        #averaged_sets: 50
     
        # [11,11] works well for dl1r
        #plot_size: [11, 11]
